#include <stdio.h>
/* Copie l'entrée sur la sortie (version 1) */
int main()
{
	char c;
	c = getchar();
	while ( c != EOF )
	{
		putchar(c);
		c = getchar();
	}
	return 0;
}
set key left box
set xlabel "Number of elements"
set ylabel "Sorting time (nanoseconds)
set format y "%10.0f"
set terminal pdf size 21cm,14.8cm
set output "plot.pdf"
plot    "plot.dat" using 1:2 title 'Merge Sort' with linespoints , \
        "plot.dat" using 1:3 title 'STL Sort' with linespoints , \
        "plot.dat" using 1:4 title 'Insertion Sort' with linespoints
pause -1
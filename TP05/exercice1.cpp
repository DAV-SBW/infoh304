#include <iostream>
#include <string>
#include <fstream>
#include <set> 
using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::ostream;
using std::set;
using std::pair;

void printSet(const set<string> & c, ostream & out = cout) 
{
    set<string>::const_iterator itr;
    for ( itr = c.begin(); itr != c.end(); ++itr )
        out << *itr << endl;
    
}

int main(int argc, char *argv[])
{
    ifstream fichier (argv[1]);   //Fichier ouvert en écriture
    if( fichier.is_open() )
    {
        set<string> liste;
        string mot;
        while ( fichier >> mot )
            liste.insert(mot);
        fichier.close();
        printSet(liste);
        cout << "Nombre de mots unique: " << liste.size() << endl;
    }
    else
    {
        cout << "Impossible d'ouvrir le fichier" << endl;
        return 1;
    }
    return 0;
}